import React from 'react';
import ReactDOM from 'react-dom';


//Ejemplo1 retornando JSX
const App = () => {
    return (
        //Este h1 no es HTML es JSX
        <h1>Creado con JSX</h1>
        )
}
//Esta linea especifica que debe renderizar, recibe como 1 argumento que debe renderizar y como segundo argumento donde lo debe hacer
ReactDOM.render(<App/>, document.querySelector('#contenedor'))


//Ejemplo2 usando react para crear elemento
const App2 = () => {
    return React.createElement('h1', {className:'titulo'}, 'Elemento creado con createElement de React ')
}
ReactDOM.render(<App2/>, document.querySelector('#contenedor2'))

//Ejemplo3 usando react para crear elemento con sub elementos
const App3 = () => {
    return React.createElement('h1', {className:'titulo'}, React.createElement('h3', {className:'subTitulo'},  'Subelemento Creado con CreateElement'))
}
ReactDOM.render(<App3/>, document.querySelector('#contenedor3'))

//Ejemplo4 usando JSX retorna mulitples elementos
const App4 = () => {
    return (
        <div className = 'nuevoElemento'>
            <h1>Nuevo Elemento</h1>
            <p>Parrafo dentro de Class = nuevoElemento</p>
        </div>
    )
}
ReactDOM.render(<App4/>, document.querySelector('#contenedor4'))